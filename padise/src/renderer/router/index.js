import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/trivia/language',
      name: 'trivia-page',
      component: require('@/components/TriviaPage').default
    },
    {
      path: '/puzzle/',
      name: 'puzzle-page',
      component: require('@/components/PuzzlePage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.afterEach((to, from) => {
  if (to.name !== 'landing-page') {
    document.querySelector('body').classList.add('unblurred-bg')
  } else {
    document.querySelector('body').classList.remove('unblurred-bg')
  }
})

export default router
