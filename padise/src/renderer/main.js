import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import IdleVue from 'idle-vue'
import Vue2TouchEvents from 'vue2-touch-events'
/* import VueTouch from 'vue-touch' */

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

const eventsHub = new Vue()

Vue.use(Vue2TouchEvents, {
  swipeTolerance: 150
})

Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 900000
})

/* Vue.use(VueTouch) */

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
  webPreferences: {
    webSecurity: false
  },
  /* render: h => h(App), */
  onIdle () {
    if (this.$route.name !== 'landing-page') {
      router.go(-1)
    }
  }
}).$mount('#app')

var webFrame = require('electron').webFrame
webFrame.setVisualZoomLevelLimits(1, 1)
webFrame.setLayoutZoomLevelLimits(0, 0)
